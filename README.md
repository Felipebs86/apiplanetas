# API Planetas Star Wars #

Instruções para uso da API

### Requerimentos ###

* Java 
* Mongodb

### Como utilizar ###

* Faça o clone do projeto
* Importe para a ide como um projeto maven
* Tenha o mongoDB instalado e rodando
* Abrir o projeto e executa-lo como Spring Boot App

### EndPoints ###

* localhost:8080/api/planetas (POST)
> Cadastro de planetas

> Passar o json no body seguindo o modelo:
```javascript
{
	"nome": "texto", (Required)
	"clima": "texto", (Required)
	"terreno": "texto" (Required)
}
```

* localhost:8080/api/planetas/ (GET)
> Visualizar todos os planetas salvos, não é necessário passar nenhum dado no body

* localhost:8080/api/planetas/{parametro} (GET)
> Busca um planeta específico por ID ou NOME, não é necessário passar nenhum dado no body

* localhost:8080/api/planetas/{parametro} (DELETE)
> Deleta o planeta passado por ID ou NOME no parametro

### DOCUMENTAÇÃO SWAGGER ###

* localhost:8080/swagger-ui.html
> endpoint para verificar a documentação e realizar testes na api

### COLLECTION POSTMAN ###

* https://www.getpostman.com/collections/5b425548b4aea1b50058

