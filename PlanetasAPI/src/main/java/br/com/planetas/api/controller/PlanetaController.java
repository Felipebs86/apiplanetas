package br.com.planetas.api.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.planetas.api.documents.Planeta;
import br.com.planetas.api.documents.RequestCadastro;
import br.com.planetas.api.model.Result;
import br.com.planetas.api.seq.services.SequenciaService;
import br.com.planetas.api.services.PlanetaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "api/planetas")
@Api(value="Planetas", description="API para cadastro e leitura de Planetas do Star Wars")
public class PlanetaController {

    @Autowired
    private PlanetaService planetaService;

    @Autowired
    private SequenciaService sequenciaService;

    @ApiOperation(value="Listagem de planetas cadastrados")
    @GetMapping
    public ResponseEntity<List<Planeta>> listarPlanetas() {
        return ResponseEntity.ok(this.planetaService.listarPlanetas());
    }

    @ApiOperation(value="Listagem dos planetas cadastrados por nome ou id")
    @GetMapping(path = "/{parametro}")
    public ResponseEntity<Object> listarPorParametro(@PathVariable(name = "parametro") String parametro) {
        if (parametro.matches("[0-9]+")) {
        	if(this.planetaService.encontraNoBancoPorId(parametro) == null){
                return ResponseEntity.badRequest().body("Planeta ID " + parametro + " não existe no banco");
            }
            return ResponseEntity.ok(this.planetaService.listarPorId(parametro));
        }
        if(this.planetaService.encontraNoBancoPorId(parametro) == null){
            return ResponseEntity.badRequest().body("Planeta nome " + parametro + " não existe no banco");
        }
        return ResponseEntity.ok(this.planetaService.listarPorNome(parametro));
    }

    @ApiOperation(value="Cadastro de planetas")
    @PostMapping
    public ResponseEntity<Object> adicionar(@Valid @RequestBody RequestCadastro planetaRecebido, BindingResult result) {
    	if(result.hasErrors()) {
    		return ResponseEntity.badRequest().body("Request invalido: todas as informações devem ser preenchidas");
    	}
    	
        String nomeRecebido = planetaRecebido.getNome();
        int aparicoes =  consultaPlaneta(nomeRecebido);
        Planeta existeNoBanco = this.planetaService.encontraNoBancoPorNome(nomeRecebido.toLowerCase());

        if(aparicoes == -1){
            return ResponseEntity.badRequest().body("Planeta "+ planetaRecebido.getNome().toLowerCase() +" inexistente no universo Star Wars");
        } else if(existeNoBanco != null){
            return ResponseEntity.badRequest().body("Planeta "+ planetaRecebido.getNome().toLowerCase() +" já salvo no banco");
        }
        
        Planeta planetaNovo = new Planeta();
        String id = retornaIdSequencial();
        planetaNovo.setId(id);
        planetaNovo.setNome(nomeRecebido.toLowerCase());
        planetaNovo.setClima(planetaRecebido.getClima().toLowerCase());
        planetaNovo.setTerreno(planetaRecebido.getTerreno().toLowerCase());
        planetaNovo.setAparicoes(String.valueOf(aparicoes));

        this.planetaService.adicionar(planetaNovo);
        return ResponseEntity.ok("Planeta " + planetaNovo.getNome() + " salvo");
    }

    private int consultaPlaneta(String nome) throws RestClientException {
        RestTemplate rt = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = "https://swapi.co/api/planets/?search=" + nome;
        ResponseEntity<Result> res = rt.exchange(url, HttpMethod.GET, entity, Result.class);
        Long existe = res.getBody().getCount();
        
        if(existe > 0){
            int aparicoesEmFilmes = res.getBody().getResults().get(0).filmsUrls.size();
            return aparicoesEmFilmes;
        }
        return -1;
    }

    private String retornaIdSequencial() {
        if (!sequenciaService.existeSequencia()) {
            sequenciaService.iniciaSequencia();
        }
        return sequenciaService.retornaProxSequencia();
    }

    @ApiOperation(value="Exclusão de planetas do banco por nome ou id")
    @DeleteMapping(path = "/{parametro}")
    public ResponseEntity<Object> excluirPorParametro(@PathVariable(name = "parametro") String parametro) {
        if (parametro.matches("[0-9]+")) {
            if(this.planetaService.encontraNoBancoPorId(parametro) == null){
                return ResponseEntity.badRequest().body("Planeta ID " + parametro + " não existe no banco");
            }
            this.planetaService.excluirPorId(parametro);
            return ResponseEntity.ok("Planeta ID " + parametro + " removido");
        }
        if(this.planetaService.encontraNoBancoPorNome(parametro) == null){
            return ResponseEntity.badRequest().body("Planeta " + parametro + " não existe no banco");
        }
        this.planetaService.excluirPorNome(parametro);
        return ResponseEntity.ok("Planeta nome " + parametro + " removido");
    }

}
