package br.com.planetas.api.seq.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sequencia")
public class Sequencia {

	@Id
	private String id;
	private Long seq;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}

	@Override
	public String toString() {
		return "Sequencia Id [id=" + id + ", seq=" + seq + "]";
	}
}
