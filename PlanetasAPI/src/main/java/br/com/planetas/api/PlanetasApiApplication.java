package br.com.planetas.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanetasApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlanetasApiApplication.class, args);
	}
}
