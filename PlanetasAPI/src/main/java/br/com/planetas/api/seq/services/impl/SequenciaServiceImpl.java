package br.com.planetas.api.seq.services.impl;

import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.planetas.api.seq.repository.SequenciaRepository;
import br.com.planetas.api.seq.services.SequenciaService;

@Service
public class SequenciaServiceImpl implements SequenciaService {
	
	@Autowired
	private SequenciaRepository repositorio;

	@Override
	public String retornaProxSequencia() {
		return this.repositorio.retornaProxSequencia();
	}

	@Override
	public Boolean existeSequencia() {
		return this.repositorio.existeSequencia();
	}

	@Override
	public void iniciaSequencia() {
		this.repositorio.iniciaSequencia();
	}

}
