package br.com.planetas.api.seq.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;


import br.com.planetas.api.seq.document.Sequencia;

public class SequenciaRepositoryImpl implements SequenciaRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public String retornaProxSequencia() {
		Query query = new Query(Criteria.where("id").is("contador"));
	
		Update update = new Update();
		update.inc("seq", 1);

		FindAndModifyOptions options = new FindAndModifyOptions();
		options.returnNew(true);

		Sequencia seqId = mongoTemplate.findAndModify(query, update, options, Sequencia.class);

		return String.valueOf(seqId.getSeq());
	}
	
	@Override
	public Boolean existeSequencia() {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is("contador"));
		return this.mongoTemplate.exists(query, Sequencia.class);
	}

	@Override
	public void iniciaSequencia() {
		Sequencia seq = new Sequencia();
		seq.setId("contador");
		seq.setSeq(0);
		this.mongoTemplate.save(seq);
	}


}
