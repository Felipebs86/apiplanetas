package br.com.planetas.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import br.com.planetas.api.documents.Planeta;
import br.com.planetas.api.repositories.PlanetaRepository;
import br.com.planetas.api.services.PlanetaService;

@Service
public class PlanetaServiceImpl implements PlanetaService {

    @Autowired
    private PlanetaRepository planetaRepository;

    @Override
    public List<Planeta> listarPlanetas() {
        return this.planetaRepository.listarTodos();
    }

    @Override
    public Planeta listarPorId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return (Planeta) this.planetaRepository.listarPor(query);
    }

    @Override
    public Planeta listarPorNome(String nome) {
        Query query = new Query();
        query.addCriteria(Criteria.where("nome").is(nome));
        return (Planeta) this.planetaRepository.listarPor(query);
    }

    @Override
    public void adicionar(Planeta planeta) {
        this.planetaRepository.salvar(planeta);
    }

    @Override
    public void excluirPorId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        this.planetaRepository.excluirPor(query);
    }

    @Override
    public void excluirPorNome(String nome) {
        Query query = new Query();
        query.addCriteria(Criteria.where("nome").is(nome));
        this.planetaRepository.excluirPor(query);
    }

    @Override
    public Planeta encontraNoBancoPorNome(String nome) {
        Query query = new Query();
        query.addCriteria(Criteria.where("nome").is(nome));
        return this.planetaRepository.existeNoBanco(query);
    }

    @Override
    public Planeta encontraNoBancoPorId(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return this.planetaRepository.existeNoBanco(query);
    }

}
