package br.com.planetas.api.seq.repository;

import org.springframework.data.mongodb.core.MongoTemplate;

public interface SequenciaRepositoryCustom {
	
	String retornaProxSequencia();
	
	Boolean existeSequencia();
	
	void iniciaSequencia();

}
