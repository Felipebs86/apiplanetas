package br.com.planetas.api.repositories.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import br.com.planetas.api.documents.Planeta;
import br.com.planetas.api.repositories.PlanetaRepositoryCustom;

public class PlanetaRepositoryCustomImpl implements PlanetaRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Planeta> listarTodos() {
        return this.mongoTemplate.findAll(Planeta.class);
    }

    @Override
    public Planeta listarPor(Query query) {
        return this.mongoTemplate.findOne(query, Planeta.class);
    }

    @Override
    public void salvar(Planeta planeta) {
        this.mongoTemplate.save(planeta);

    }

    @Override
    public void excluirPor(Query query) {
        this.mongoTemplate.remove(query, Planeta.class);

    }

    @Override
    public Planeta existeNoBanco(Query query) {
        return this.mongoTemplate.findOne(query, Planeta.class);
    }

}
