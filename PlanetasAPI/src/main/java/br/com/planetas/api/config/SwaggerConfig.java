package br.com.planetas.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("br.com.planetas.api.controller"))
                .paths(regex("/api/planetas.*"))
                .build()
                .apiInfo(metaData());
             
    }
    
    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "Desafio B2W",
                "API de planetas do universo Star Wars",
                "1.0",
                "felipebahiensesantos@gmail.com",
                "Felipe Bahiense",
                "",
                "");
        return apiInfo;
    }
}
