package br.com.planetas.api.seq.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.planetas.api.repositories.PlanetaRepositoryCustom;
import br.com.planetas.api.seq.document.Sequencia;

public interface SequenciaRepository extends MongoRepository<Sequencia, Long>, SequenciaRepositoryCustom {

}
