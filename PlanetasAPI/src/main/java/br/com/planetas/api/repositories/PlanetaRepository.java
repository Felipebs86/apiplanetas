package br.com.planetas.api.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.planetas.api.documents.Planeta;

public interface PlanetaRepository extends MongoRepository<Planeta, String>, PlanetaRepositoryCustom{

}
