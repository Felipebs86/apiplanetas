package br.com.planetas.api.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
    
    private Long count;
    private String next;
    private String previous;
    private ArrayList<Planet> results;
    public Long getCount() {
        return count;
    }
    public void setCount(Long count) {
        this.count = count;
    }
    public String getNext() {
        return next;
    }
    public void setNext(String next) {
        this.next = next;
    }
    public String getPrevious() {
        return previous;
    }
    public void setPrevious(String previous) {
        this.previous = previous;
    }
    public ArrayList<Planet> getResults() {
        return results;
    }
    public void setResults(ArrayList<Planet> results) {
        this.results = results;
    }
    
    
    
}
