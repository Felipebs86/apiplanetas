package br.com.planetas.api.documents;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Document(collection = "Planeta")
public class Planeta {
	
	@ApiModelProperty(notes = "ID sequencial", required = true)
	private String id;
	@ApiModelProperty(notes = "Nome do planeta", required = true)
	private String nome;
	@ApiModelProperty(notes = "Clima do planeta", required = true)
	private String clima;
	@ApiModelProperty(notes = "Tipo de terreno do planeta", required = true)
	private String terreno;
	@ApiModelProperty(notes = "Quantidade de aparições em filmes do Star Wars", required = true)
	private String aparicoes;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getClima() {
		return clima;
	}
	
	public void setClima(String clima) {
		this.clima = clima;
	}
	
	public String getTerreno() {
		return terreno;
	}
	
	public void setTerreno(String terreno) {
		this.terreno = terreno;
	}
	
	public String getAparicoes() {
		return aparicoes;
	}
	
	public void setAparicoes(String aparicoes) {
		this.aparicoes = aparicoes;
	}
	
}
