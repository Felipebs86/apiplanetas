package br.com.planetas.api.seq.services;

public interface SequenciaService {
	
	String retornaProxSequencia();
	
	Boolean existeSequencia();
	
	void iniciaSequencia();

}
