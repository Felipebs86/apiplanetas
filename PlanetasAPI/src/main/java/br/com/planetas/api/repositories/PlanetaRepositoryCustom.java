package br.com.planetas.api.repositories;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;

import br.com.planetas.api.documents.Planeta;

public interface PlanetaRepositoryCustom {
	
	List<Planeta> listarTodos();
	
	Planeta listarPor(Query query);

	void salvar(Planeta planeta);

	void excluirPor(Query query);
	
	Planeta existeNoBanco(Query query);

}
