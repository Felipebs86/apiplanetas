package br.com.planetas.api.services;

import java.util.List;

import br.com.planetas.api.documents.Planeta;

public interface PlanetaService {
	
	List<Planeta> listarPlanetas();
	
	Planeta listarPorId(String id);
	
	Planeta listarPorNome(String nome);
	
	void adicionar(Planeta planeta);
	
	void excluirPorId(String id);
	
	void excluirPorNome(String nome);
	
	Planeta encontraNoBancoPorNome(String nome);
	
	Planeta encontraNoBancoPorId(String id);

}
