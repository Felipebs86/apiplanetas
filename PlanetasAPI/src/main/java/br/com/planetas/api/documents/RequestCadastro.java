package br.com.planetas.api.documents;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class RequestCadastro {
	
	@ApiModelProperty(notes = "Nome do planeta", required = true)
	@NotEmpty
	@NotBlank
	private String nome;
	@ApiModelProperty(notes = "Clima do planeta", required = true)
	@NotEmpty
	@NotBlank
	private String clima;
	@ApiModelProperty(notes = "Tipo de terreno do planeta", required = true)
	@NotEmpty
	@NotBlank
	private String terreno;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getClima() {
		return clima;
	}
	
	public void setClima(String clima) {
		this.clima = clima;
	}
	
	public String getTerreno() {
		return terreno;
	}
	
	public void setTerreno(String terreno) {
		this.terreno = terreno;
	}
	
}
